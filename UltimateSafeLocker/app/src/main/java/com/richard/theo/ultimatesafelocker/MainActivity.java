package com.richard.theo.ultimatesafelocker;

import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.ParcelUuid;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private CredentialsHandler credentialsHandler;

    private boolean mIsConnect = true;
    private BluetoothDevice mDevice;
    private BluetoothA2dp mBluetoothA2DP;

    private BluetoothAdapter bluetoothAdapter;
    private boolean isBluetoothConnected = false;
    private boolean isBluetoothEnabled = false;
    private TextView bluetoothTextView;
    private RelativeLayout bluetoothInitLayout;
    private RelativeLayout passwordInitLayout;
    private TextView passwordText;
    private EditText passwordEdit;
    private Button passwordButton;

    private String keyUUID;
    private boolean keySet = false;

    private boolean isPasswordSet = true;

    private int bluetoothstep = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        credentialsHandler = CredentialsHandler.getInstance(this);
        if (credentialsHandler.tryGetKeyUUID()) {
            keySet = true;
            keyUUID = credentialsHandler.getKeyUUID();
            Log.d("keyUUID","KEYUUID : " + keyUUID);
        }
        else {
            Log.d("keyUUID", "is not set");
        }

        isPasswordSet = credentialsHandler.tryGetPassword();
        Log.d("isPassWordSet", "isPassWordSet : " + isPasswordSet);

        passwordText = (TextView) findViewById(R.id.PasswordText);
        passwordEdit = (EditText) findViewById(R.id.PasswordEdit);
        passwordButton = (Button) findViewById(R.id.PasswordButton);

        passwordButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (isPasswordSet)
                    tryPassword();
                else
                    setPassword();
            }
        });

        bluetoothTextView = (TextView) findViewById(R.id.BluetoothText);

        passwordInitLayout = (RelativeLayout) findViewById(R.id.PasswordInitLayout);

        bluetoothInitLayout = (RelativeLayout) findViewById(R.id.BluetoothInitLayout);
        bluetoothInitLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Log.d("click", "click");
                if (bluetoothstep == 0)
                    enableBluetooth();
                else if (bluetoothstep == 1)
                    connectToDevice();
                else
                    return;
            }
        });

        initBluetooth();
        initBroadCastReceiver();
        setUpBluetooth();

    }

    private void setUpBluetooth() {
        //displayPassword();

        if (isBluetoothConnected) {
            displayPassword();
        } else if (isBluetoothEnabled) {
            displayConnectToDevice();
        } else {
            displayEnableBluetooth();
        }

    }

    private void tryKeyUUID(ParcelUuid[] try_key)
    {
        UUID uuid = try_key[0].getUuid();
        long most = uuid.getMostSignificantBits();
        long least = uuid.getLeastSignificantBits();

        if (!keySet) {
            credentialsHandler.saveKeyUUID(make_key(most, least));
            Log.d("tryKeyUUID", "KEY SAVED");
            keyUUID = make_key(most, least);
            isBluetoothConnected = true;
            Toast.makeText(this, "New device connected.",
                    Toast.LENGTH_SHORT).show();
            displayPassword();
        } else {
            Log.d("tryKeyUUID","keyUUID : " + keyUUID);
            Log.d("tryKeyUUID","tryKey : " + make_key(most, least));
            if (keyUUID.equals(make_key(most, least))) {
                Log.d("tryKeyUUID", "KEY OK");
                isBluetoothConnected = true;
                Toast.makeText(this, "Device connected.",
                        Toast.LENGTH_SHORT).show();
                displayPassword();
            }
        }
        Log.d("tryKey", "key : " + most + "/" + least);
    }

    private String make_key(long most, long least) {
        return ("" + most + "/" + least);
    }

    private void displayEnableBluetooth() {
        bluetoothTextView.setText("Please\nEnable Bluetooth");
        bluetoothstep = 0;
    }

    private void enableBluetooth() {
        bluetoothAdapter.enable();
        displayConnectToDevice();
    }

    private void displayConnectToDevice() {
        if (keySet)
            bluetoothTextView.setText("Connect to key Device");
        else
            bluetoothTextView.setText("Choose a key Device");
        bluetoothstep = 1;
    }

    private void connectToDevice() {
        startActivity(new Intent(Settings.ACTION_BLUETOOTH_SETTINGS));
    }

    private void displayPassword() {
        //bluetoothTextView.setText("Enter Password");
        bluetoothInitLayout.setVisibility(View.INVISIBLE);
        passwordInitLayout.setVisibility(View.VISIBLE);
        bluetoothstep = 2;
        if (!isPasswordSet) {
            passwordText.setText("Please,\nChoose your Password");
        }
        else {
            passwordText.setText("Please,\nEnter your password");
        }
    }

    private void tryPassword() {
        String pass = passwordEdit.getText().toString();
        Log.d("trypassword", "password is : " + pass);
        if (credentialsHandler.tryPassword(pass)) {
            Log.d("Connected", "WE ARE CONNECTED");
            Intent intent = new Intent(this, SafeLockerActivity.class);
            startActivity(intent);
        }
        else {
            Toast.makeText(this, "Wrong Password.",
                    Toast.LENGTH_SHORT).show();
            Log.d("Connected", "PASSWORD IS INVALID");
        }
    }

    private void setPassword() {
        String pass = passwordEdit.getText().toString();
        Log.d("setpassword", "password is : " + pass);
        credentialsHandler.savePassword(pass);
        Toast.makeText(this, "Password Saved.",
                Toast.LENGTH_SHORT).show();
    }

    private void initBluetooth() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        int profileConnectionState = bluetoothAdapter.getProfileConnectionState(BluetoothProfile.A2DP);
//        Log.d("bluetooth","profile is " + profileConnectionState);

        isBluetoothEnabled = bluetoothAdapter.isEnabled();


        switch (profileConnectionState){
            case(BluetoothProfile.STATE_CONNECTED):
                Log.d("already paired","already paired");
                testConnect();
                break;
            case(BluetoothProfile.STATE_CONNECTING):
                //isBluetoothConnected = true;
                break;
            case(BluetoothProfile.STATE_DISCONNECTED):
                isBluetoothConnected = false;
                break;
            case(BluetoothProfile.STATE_DISCONNECTING):
                isBluetoothConnected = false;
                break;
        }


        Log.d("bluetooth","is profile connected : " + isBluetoothConnected);
        Log.d("bluetooth","is enabled : " + isBluetoothEnabled);

        //bluetoothAdapter.getProfileProxy(this, mProfileListener, BluetoothProfile.A2DP);
    }

    private void testConnect() {
        Log.d("testConnect","testConnect");
        Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();
        Iterator<BluetoothDevice> iterator = devices.iterator();
        if (iterator.hasNext()) {
            BluetoothDevice device = iterator.next();
            tryKeyUUID(device.getUuids());
        }
    }

    private void initBroadCastReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        this.registerReceiver(mReceiver, filter);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                //Device found
            }
            else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                //check if its the right device
                tryKeyUUID(device.getUuids());
            }
            else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                //Done searching
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {
                //Device is about to disconnect
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                //Device has disconnected
            }
        }
    };

    private BluetoothProfile.ServiceListener mProfileListener = new BluetoothProfile.ServiceListener() {
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            if (profile == BluetoothProfile.A2DP) {
                mBluetoothA2DP = (BluetoothA2dp) proxy;
                try {
                    if (mIsConnect) {
                        Log.d("bluetooth", "connected");
                        //isBluetoothConnected = true;
//                        Method connect = BluetoothA2dp.class.getDeclaredMethod("connect", BluetoothDevice.class);
//                        connect.setAccessible(true);
//                        connect.invoke(mBluetoothA2DP, mDevice);
                    } else {
                        isBluetoothConnected = false;
//                        Method disconnect = BluetoothA2dp.class.getDeclaredMethod("disconnect", BluetoothDevice.class);
//                        disconnect.setAccessible(true);
//                        disconnect.invoke(mBluetoothA2DP, mDevice);
                    }
                }catch (Exception e){
                } finally {
                }
            }
        }
        public void onServiceDisconnected(int profile) {
            Log.d("bluetooth", "disconnected");
        }
    };

}
