package com.richard.theo.ultimatesafelocker;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.File;
import java.io.IOException;

/**
 * Created by Theo on 04/05/2017.
 */

public class CredentialsHandler {

    public static CredentialsHandler instance = null;
    private Context context;
    private SharedPreferences settings;

    private String KeyUUIDFile = "KeyUUIDFile";


    public CredentialsHandler(Context ctx) {
        context = ctx;
        settings = context.getSharedPreferences(KeyUUIDFile, 0);
    }

    public static CredentialsHandler getInstance(Context ctx) {
        if (instance == null)
            instance = new CredentialsHandler(ctx);
        return instance;
    }

    public void savePassword(String pass) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("password", pass);
        editor.putBoolean("passwordSet", true);
        editor.commit();
        Log.d("password", "password is saved");
    }

    public boolean tryGetPassword() {
        boolean result = settings.getBoolean("passwordSet", false);
        return result;
    }

    public boolean tryPassword(String pass) {
        if (!tryGetPassword())
            return false;
        String password = settings.getString("password", "null");
        if (password != "null" && password.equals(pass))
            return true;
        return false;
    }

    public void saveKeyUUID(String key) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("keyUUID", key);
        editor.putBoolean("keySet", true);
        editor.commit();
    }

    public boolean tryGetKeyUUID() {
        boolean result = settings.getBoolean("keySet", false);
        return result;
    }

    public String getKeyUUID() {
        String key = settings.getString("keyUUID", "null");
        return key;
    }

    public void saveFilePath(String path) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("filePath",path);
        editor.commit();
    }

    public String getFilePath() {
        String path = settings.getString("filePath", "null");
        return (path);
    }
}
